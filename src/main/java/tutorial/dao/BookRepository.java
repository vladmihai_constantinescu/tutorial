package tutorial.dao;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import tutorial.model.Book;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BookRepository {

    private static final Map<Long, Book> books = Maps.newHashMap();

    public static Book addBook(Book book) {
        books.put(book.getId(), book);
        return book;
    }

    public static List<Book> listBooks(){
        return books.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    public static Book updateBook(long id, String name) {
        Book book = books.get(id);

        book.setName(name);

        return book;
    }

    public static Book delete(long id) {
        return books.remove(id);
    }
}
