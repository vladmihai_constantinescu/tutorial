package tutorial.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tutorial.dao.BookRepository;
import tutorial.model.Book;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Controller
@RequestMapping("/books")
public class BookController {

    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/list")
    @ResponseBody
    public List<Book> listBooks(){
        return BookRepository.listBooks();
    }

    @PostMapping("/add")
    @ResponseBody
    public Book add(@RequestParam(name = "name", required = true, defaultValue = "New Book") String name) {
//        return BookRepository.addBook(new Book(counter.incrementAndGet(), name));
        return BookRepository.addBook(new Book((long) (Math.random()*100), name));
    }

    @PostMapping("/update")
    @ResponseBody
    public Book update(@RequestParam(name = "id", required = true) long id,
                           @RequestParam(name = "name", required = true) String name){

        return BookRepository.updateBook(id, name);
    }

    @PostMapping("/delete")
    @ResponseBody
    public Book delete(@RequestParam(name = "id", required = true) long id){

        return BookRepository.delete(id);
    }
}
